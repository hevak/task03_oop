package com.epam.edu.online.controller;

import com.epam.edu.online.model.Apartment;

import java.util.List;

public interface Controller {

    void refillListApartment();
    List<Apartment> getApartments();
    List<Apartment> getRentApartments();
    List<Apartment> getPurchaseApartments();

    List<Apartment> cheaperThan(int maxPrice);
    List<Apartment> expensiveThan(int minPrice);

    List<Apartment> roomCount(int count);
    List<Apartment> shapeMin(double minShape);
    List<Apartment> shapeMax(double maxShape);
}
