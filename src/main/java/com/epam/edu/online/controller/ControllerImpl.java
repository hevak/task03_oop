package com.epam.edu.online.controller;

import com.epam.edu.online.model.Apartment;
import com.epam.edu.online.model.BuisnessLogic;
import com.epam.edu.online.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new BuisnessLogic();
    }


    @Override
    public List<Apartment> getApartments() {
        return model.getApartments();
    }

    @Override
    public void refillListApartment() {
        model.refillListApartments();
    }

    @Override
    public List<Apartment> getRentApartments() {
        return model.getRentApartments();
    }

    @Override
    public List<Apartment> getPurchaseApartments() {
        return model.getPurchaseApartments();
    }

    @Override
    public List<Apartment> cheaperThan(int maxPrice) {
        return model.getCheaperThan(maxPrice);
    }

    @Override
    public List<Apartment> expensiveThan(int minPrice) {
        return model.getExpensiveThan(minPrice);
    }

    @Override
    public List<Apartment> roomCount(int count) {
        return model.getRoomCount(count);
    }

    @Override
    public List<Apartment> shapeMin(double minShape) {
        return model.getShapeMin(minShape);

    }

    @Override
    public List<Apartment> shapeMax(double maxShape) {
        return model.getShapeMax(maxShape);
    }
}
