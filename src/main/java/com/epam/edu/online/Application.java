package com.epam.edu.online;

import com.epam.edu.online.model.Apartment;
import com.epam.edu.online.model.ContractType;
import com.epam.edu.online.model.Flat;
import com.epam.edu.online.view.ConsoleView;

/**
 * This class contain main method.
 * @author Oleh Hevak
 */
public final class Application {
    /**
     * Utility class cannot have
     * public or default constructor.
     */
    private Application() {
    }
    /**
     * The application's entry point.
     *
     * @param args an array of command-line arguments for the application
     */
    public static void main(final String[] args) {
        new ConsoleView().show();
    }
}
