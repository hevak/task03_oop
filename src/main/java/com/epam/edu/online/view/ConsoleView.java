package com.epam.edu.online.view;



import com.epam.edu.online.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This is class for view
 */
public class ConsoleView {

    private ControllerImpl controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public ConsoleView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("all", "`all`          - print apartments");
        menu.put("rent", "`rent`         - print rent apartments");
        menu.put("purchase", "`purchase`     - print purchase apartments");
        menu.put("room", "`room`     - print count room");
        menu.put("shapeMin", "`shapeMin`     - print min shape");
        menu.put("shapeMax", "`shapeMax`     - print max shape");
        menu.put("cheaper", "`cheaper`      - print cheaper than ...");
        menu.put("expensive", "`expensive`    - print more expensive than ...");
        menu.put("refill", "`refill`       - refill apartments");
        menu.put("help", "`help`         - help");
        menu.put("Q", "`Q`            - EXIT");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("all", this::allApartment);
        methodsMenu.put("rent", this::rentApartment);
        methodsMenu.put("purchase", this::purchaseApartment);
        methodsMenu.put("refill", this::refillApartment);
        methodsMenu.put("shapemin", this::shapeMin);
        methodsMenu.put("shapemax", this::shapeMax);
        methodsMenu.put("room", this::roomCount);
        methodsMenu.put("cheaper", this::cheaperThan);
        methodsMenu.put("expensive", this::expensiveThan);



        methodsMenu.put("help", this::outputMenu);
    }

    private void roomCount() {
        System.out.print("Enter numerical value: ");
        controller.roomCount(input.nextInt()).forEach(System.out::println);
    }

    private void shapeMax() {
        System.out.print("Enter numerical value: ");
        controller.shapeMax(input.nextDouble()).forEach(System.out::println);
    }

    private void shapeMin() {
        System.out.print("Enter numerical value: ");
        controller.shapeMin(input.nextDouble()).forEach(System.out::println);
    }

    private void allApartment() {
        controller.getApartments().forEach(System.out::println);
    }

    private void rentApartment() {
        controller.getRentApartments().forEach(System.out::println);
    }

    private void purchaseApartment() {
        controller.getPurchaseApartments().forEach(System.out::println);
    }

    private void refillApartment() {
        controller.refillListApartment();
    }




    private void cheaperThan() {
        System.out.print("Enter numerical value: ");
        controller.cheaperThan(input.nextInt()).forEach(System.out::println);
    }

    private void expensiveThan() {
        System.out.print("Enter numerical value: ");
        controller.expensiveThan(input.nextInt()).forEach(System.out::println);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * method for showing message to write a command
     */
    public void show() {
        String keyMenu;
        do {
            System.out.print("Please, enter command: ");
            keyMenu = input.nextLine().toLowerCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
//                System.out.println("Wrong command, try one more time or write command `help`");
            }
        } while (!keyMenu.equals("q"));
    }
}
