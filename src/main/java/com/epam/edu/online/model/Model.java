package com.epam.edu.online.model;

import java.util.List;

public interface Model {

    List<Apartment> refillListApartments();

    List<Apartment> getApartments();

    List<Apartment> getRentApartments();

    List<Apartment> getPurchaseApartments();

    List<Apartment> getCheaperThan(int maxPrice);

    List<Apartment> getExpensiveThan(int minPrice);

    List<Apartment> getRoomCount(int count);

    List<Apartment> getShapeMin(double minShape);

    List<Apartment> getShapeMax(double maxShape);
}
