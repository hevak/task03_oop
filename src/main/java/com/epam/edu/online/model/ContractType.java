package com.epam.edu.online.model;

public enum ContractType {
    RENT, PURCHASE
}
