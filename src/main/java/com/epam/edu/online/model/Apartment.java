package com.epam.edu.online.model;

public abstract class Apartment {

    private int price;
    private ContractType contractType;
    private String address;
    private Double shape;
    private int countRoom;

    public Apartment(ContractType contractType, int price, String address, Double shape, int countRoom) {
        this.contractType = contractType;
        this.price = price;
        this.address = address;
        this.shape = shape;
        this.countRoom = countRoom;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getShape() {
        return shape;
    }

    public void setShape(Double shape) {
        this.shape = shape;
    }

    public int getCountRoom() {
        return countRoom;
    }

    public void setCountRoom(int countRoom) {
        this.countRoom = countRoom;
    }

    @Override
    public String toString() {
        return "price=" + price +
                ", address='" + address + '\'' +
                ", shape=" + shape +
                ", countRoom=" + countRoom;
    }
}
