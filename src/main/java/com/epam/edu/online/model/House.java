package com.epam.edu.online.model;

public class House extends Apartment {

    private int countFloor;

    public House(ContractType contractType, int price, String address, Double shape, int countRoom, int countFloor) {
        super(contractType, price, address, shape, countRoom);
        this.countFloor = countFloor;
    }

    public int getCountFloor() {
        return countFloor;
    }

    public void setCountFloor(int countFloor) {
        this.countFloor = countFloor;
    }

    @Override
    public String toString() {
        return "House{" + super.toString() +
                ", "+
                "countFloor=" + countFloor +
                '}';
    }
}
