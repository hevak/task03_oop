package com.epam.edu.online.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class contain all method functionality.
 */
public class BuisnessLogic implements Model {

    private List<Apartment> apartments;
    public BuisnessLogic(){
        fillListApartments();
    }

    private List<Apartment> fillListApartments() {
        this.apartments = new ArrayList<>(Arrays.asList(
                new Flat(ContractType.RENT, 300, "lviv, naukova, 33/17", 55.5, 2, 4),
                new Flat(ContractType.RENT, 200, "lviv, naukova, 35/17", 60.5, 2, 7),
                new Flat(ContractType.PURCHASE, 30000, "lviv, naukova, 37/17", 51.5, 2, 4),
                new Flat(ContractType.PURCHASE, 32000, "lviv, naukova, 38/17", 54.5, 2, 7),
                new House(ContractType.RENT, 250, "lviv, naukova, 34", 158.5, 4, 1),
                new House(ContractType.PURCHASE, 20000, "lviv, naukova, 40", 165.5, 10, 3),
                new House(ContractType.PURCHASE, 20000, "lviv, naukova, 42", 135.5, 8, 2),
                new House(ContractType.PURCHASE, 20000, "lviv, naukova, 44", 125.5, 8, 2)
        ));
        return this.apartments;
    }

    /**
     * Refilling apartments.
     */
    @Override
    public List<Apartment> refillListApartments() {
        return fillListApartments();
    }

    /**
     * Return apartments.
     */
    @Override
    public List<Apartment> getApartments() {
        return this.apartments;
    }

    /**
     * Return rent apartments.
     */
    @Override
    public List<Apartment> getRentApartments() {
        this.apartments = apartments.stream()
                .filter(apartment -> apartment.getContractType().equals(ContractType.RENT))
                .sorted(Comparator.comparingInt(Apartment::getPrice))
                .collect(Collectors.toList());
        return this.apartments;
    }

    /**
     * Return purchase apartments.
     */
    @Override
    public List<Apartment> getPurchaseApartments() {
        this.apartments = apartments.stream()
                .filter(apartment -> apartment.getContractType().equals(ContractType.PURCHASE))
                .sorted((o1, o2) -> o2.getPrice() - o1.getPrice())
                .collect(Collectors.toList());
        return this.apartments;
    }

    /**
     * Filtering list apartments by price.
     * @param maxPrice max price apartment
     * @return filtering list
     */
    @Override
    public List<Apartment> getCheaperThan(int maxPrice) {
        this.apartments = apartments.stream()
                .filter(apartment -> maxPrice >= apartment.getPrice())
                .collect(Collectors.toList());
        return this.apartments;
    }

    /**
     * Filtering list apartments by price.
     * @param minPrice min price apartment
     * @return filtering list
     */
    @Override
    public List<Apartment> getExpensiveThan(int minPrice) {
        this.apartments = apartments.stream()
                .filter(apartment -> minPrice <= apartment.getPrice())
                .collect(Collectors.toList());
        return this.apartments;
    }

    /**
     * Filtering list apartments by room.
     * @param count count room
     * @return filtering list
     */
    @Override
    public List<Apartment> getRoomCount(int count) {
        this.apartments = apartments.stream()
                .filter(apartment -> apartment.getCountRoom() == count)
                .collect(Collectors.toList());
        return this.apartments;    }

    /**
     * Filtering list apartments by shape.
     * @param minShape min shape
     * @return filtering list
     */
    @Override
    public List<Apartment> getShapeMin(double minShape) {
        this.apartments = apartments.stream()
                .filter(apartment -> apartment.getShape() >= minShape)
                .collect(Collectors.toList());
        return this.apartments;
    }
    /**
     * Filtering list apartments by shape.
     * @param maxShape max shape
     * @return filtering list
     */
    @Override
    public List<Apartment> getShapeMax(double maxShape) {
        this.apartments = apartments.stream()
                .filter(apartment -> apartment.getShape() <= maxShape)
                .collect(Collectors.toList());
        return this.apartments;
    }
}
