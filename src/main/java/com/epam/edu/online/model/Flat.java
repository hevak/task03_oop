package com.epam.edu.online.model;

public class Flat extends Apartment {

    private int floor;

    public Flat(ContractType contractType, int price, String address, Double shape, int countRoom, int floor) {
        super(contractType, price, address, shape, countRoom);
        this.floor = floor;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Flat{" + super.toString() +
                ", " +
                "floor=" + floor +
                '}';
    }
}
